# README #
V1.0

This repo contains all of the learning experiments related to the TASC project.

## Contents: ##

* tasc_learning: directory containing python code related to the tasc project
    * datasets: directory containing code for reading generated datasets
        * generate: directory containing code for generating datasets
    * models: directory containing code for creating, saving, loading, training, and using the various models we use in our experiments
    * experiments: directory contains scripts for running experiments
* data: directory contains datasets as well as results from the experiments run on the datasets