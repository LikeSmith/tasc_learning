'''
This script will test the trained neural networks
'''

import numpy as np
import pickle
import matplotlib.pyplot as plt
from keras.models import Model
from keras.models import load_model
from scipy import signal

# test case parameters
xp_des = np.array([2.0, 0.0])
xp_0 = np.array([1.0, 0.0])
xs_0 = np.array([1.5, 0.0, 0.5, 0.0, 1.5, 0.0, 0.5, 0.0])
t_f = 100.0
del_t = 0.001
K = 0.001
params_filename = '../../data/linSys/set1_param.p'
abstractor_filename = 'models/naive_abstractor.h5'
controller_filename = 'models/naive_controller.h5'

# load parameters and generate matrices
n, A, B, C, D = pickle.load(open(params_filename, 'rb'))
sys = signal.lti(A, B, C, D)

# load models
abstractor = load_model(abstractor_filename)
controller = load_model(controller_filename)

# run simulation
t = np.arange(0.0, t_f, del_t)
xs = np.zeros((t.size, xs_0.size))
xp = np.zeros((t.size, 2))
u = np.zeros((t.size, n))

xs[0, :] = xs_0
xp[0, :] = xp_0

for i in range(1, t.size):
    xp_t = xp[i-1, :]
    xs_t = xs[i-1, :]
    xp_t1 = xp_t + K*(xp_des - xp_t)

    abs_state_t = abstractor.predict(np.array([xs_t]))
    u_t = controller.predict([abs_state_t, np.array([xp_t]), np.array([xp_t1])])

    t_i = np.array([0.0, del_t])
    u_i = np.ones((2, 4))*u_t
    x0 = np.concatenate((xs_t, xp_t))

    t_out, y, x = signal.lsim(sys, u_i, t_i, x0)

    xs[i, :] = x[1, 0:2*n]
    xp[i, :] = x[1, 2*n:]
    u[i, :] = u_t

plt.plot(t, xp)
plt.show()

plt.plot(t, u)
plt.show()

plt.plot(t, xs)
plt.show()
