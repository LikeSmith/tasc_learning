import os
import _pickle as pkl

class LinSys(object):
    setid = 1

    A = None
    B = None
    C = None
    D = None

    n_swarm_states = None
    n_swarm_inputs = None
    n_parent_states = 2
    n_abs_states = 5

    data_dir = os.path.dirname(os.path.realpath(__file__)) \
               + '/../../data/linSys/'

    def __init__(self, setid=1):
        assert(isinstance(setid, int))
        self.setid = setid

        # Load parameters
        params = pkl.load(open(self.get_param_filename(), 'rb'))
        self.n_swarm_inputs = params[0]
        self.n_swarm_states = 2*self.n_swarm_inputs
        self.A = params[0]
        self.B = params[1]
        self.C = params[2]
        self.D = params[3]

        # Load data
        data = pkl.load(open(self.get_data_filename(), 'rb'))

        self.xs_t  = data[0]
        self.xs_t1 = data[3]
        self.xp_t  = data[1]
        self.xp_t1 = data[4]
        self.u_t   = data[2]

    def get_data_filename(self):
        return self.data_dir + 'set' + str(self.setid) + '_data.p'

    def get_param_filename(self):
        return self.data_dir + 'set' + str(self.setid) + '_param.p'
