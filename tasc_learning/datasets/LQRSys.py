import os
import _pickle as p

class LQRSys(object):
    setname = 'LQR_dataset.pkl'
    data_dir = os.path.dirname(os.path.realpath(__file__)) \
               + '/../../data/LQRSys/datasets'

    def __init__(self, setname='LQR_dataset.pkl'):
        self.setname = setname

        # load data
        data = p.load(open(self.get_data_filename(), 'rb'))

        self.x_t = data[0]
        self.u = data[1]
        self.x_t1 = data[2]

    def get_data_filename(self):
        return "%s/%s"%(self.data_dir, self.setname)
