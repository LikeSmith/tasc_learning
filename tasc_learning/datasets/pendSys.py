'''
container class for loading pendulum dataset
'''

import os
import _pickle as p
import keras.backend as K
import numpy as np

class PendSys(object):
    datadir = os.path.dirname(os.path.realpath(__file__)) \
              + '/../../data/pendSys/datasets'

    def __init__(self, setname='pend_dataset1_data.pkl', parname='pend_dataset1_param.pkl'):
        self.setname = setname
        self.parname = parname

        data = p.load(open(self.get_data_filename(), 'rb'))
        params = p.load(open(self.get_param_filename(), 'rb'))

        self.x_t = data[0]
        self.u = data[1]
        self.x_t1 = data[2]

        self.m = params[0]
        self.l = params[1]
        self.c = params[2]
        self.g = params[3]
        self.del_t = 0.1
        self.u_max = 9.0

    def get_data_filename(self):
        return "%s/%s"%(self.datadir, self.setname)

    def get_param_filename(self):
        return "%s/%s"%(self.datadir, self.parname)

    def f_disc(self, args, theano_obj=True, lim_u=True):
        x, u = args

        if theano_obj:
            theta = x[:, 0]
            omega = x[:, 1]

            if lim_u:
                u = self.u_max*K.tanh(u/self.u_max)

            alpha = self.g/self.l*K.sin(theta) - self.c/(self.m*self.l**2)*omega + K.squeeze(u, axis=-1)/(self.m*self.l**2)

            omega_new = omega + self.del_t*alpha
            theta_new = theta + self.del_t*omega_new

            omega_new = K.expand_dims(omega_new)
            theta_new = K.expand_dims(theta_new)

            x_new = K.concatenate([theta_new, omega_new])
        else:
            theta = x[0]
            omega = x[1]

            if lim_u:
                u = self.u_max*np.tanh(u/self.u_max)

            alpha = self.g*self.l*np.sin(theta) - self.c/(self.m*self.l**2)*omega + u/(self.m*self.l**2)

            omega_new = omega + self.del_t*alpha
            theta_new = theta + self.del_t*omega_new

            x_new = np.array([theta_new, omega_new])

        return x_new
