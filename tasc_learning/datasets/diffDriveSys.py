import os
import _pickle as p
import keras.backend as K
import numpy as np

class diffDriveSys(object):
    data_dir = os.path.dirname(os.path.realpath(__file__)) \
               + '/../../data/diffDriveSys/datasets'

    def __init__(self, setname='diffDrive_dataset1_data.pkl', parname='diffDrive_dataset1_param.pkl'):
        self.setname = setname
        self.parname = parname

        data = p.load(open(self.get_data_filename(), 'rb'))
        self.wb, self.del_t, self.x_range, self.y_range, self.u_range = p.load(open(self.get_param_filename(), 'rb'))

    def get_data_filename(self):
        return "%s/%s"%(self.datadir, self.setname)

    def get_param_filename(self):
        return "%s/%s"%(self.datadir, self.parname)

    def f_disc(self, args, theano_obj=True):
        state, u = args

        if theano_obj:
            x = state[:, 0]
            y = state[:, 1]
            theta = state[:, 2]

            vr = state[:, 0]
            vl = state[:, 1]

            x_dot1 = 0.5*(vr + vl)*K.cos(theta)
            y_dot1 = 0.5*(vr + vl)*K.sin(theta)
            theta_dot1 = (vr - vl)/self.wb

            x_dot2 = 0.5*(vr + vl)*K.cos(theta + theta_dot1*0.5*self.del_t)
            y_dot2 = 0.5*(vr + vl)*K.sin(theta + theta_dot1*0.5*self.del_t)
            theta_dot2 = (vr - vl)/self.wb

            x_dot3 = 0.5*(vr + vl)*K.cos(theta + theta_dot2*0.5*self.del_t)
            y_dot3 = 0.5*(vr + vl)*K.sin(theta + theta_dot2*0.5*self.del_t)
            theta_dot3 = (vr - vl)/self.wb

            x_dot4 = 0.5*(vr + vl)*K.cos(theta + theta_dot3*self.del_t)
            y_dot4 = 0.5*(vr + vl)*K.sin(theta + theta_dot3*self.del_t)
            theta_dot4 = (vr - vl)/self.wb

            x_new = x + self.del_t/6.0*(x_dot1 + 2.0*x_dot2 + 2.0*x_dot3 + x_dot4)
            y_new = y + self.del_t/6.0*(y_dot1 + 2.0*y_dot2 + 2.0*y_dot3 + y_dot4)
            theta_new = theta + self.del_t/6.0*(theta_dot1 + 2.0*theta_dot2 + 2.0*theta_dot3 + theta_dot4)

            x_new = K.expand_dims(x_new)
            y_new = K.expand_dims(y_new)
            theta_new = K.expand_dims(theta_new)

            x_new = K.concatenate([x_new, y_new, theta_new])

        else:
            x = state[0]
            y = state[1]
            theta = state[2]

            vr = state[0]
            vl = state[1]

            x_dot1 = 0.5*(vr + vl)*np.cos(theta)
            y_dot1 = 0.5*(vr + vl)*np.sin(theta)
            theta_dot1 = (vr - vl)/self.wb

            x_dot2 = 0.5*(vr + vl)*np.cos(theta + theta_dot1*0.5*self.del_t)
            y_dot2 = 0.5*(vr + vl)*np.sin(theta + theta_dot1*0.5*self.del_t)
            theta_dot2 = (vr - vl)/self.wb

            x_dot3 = 0.5*(vr + vl)*np.cos(theta + theta_dot2*0.5*self.del_t)
            y_dot3 = 0.5*(vr + vl)*np.sin(theta + theta_dot2*0.5*self.del_t)
            theta_dot3 = (vr - vl)/self.wb

            x_dot4 = 0.5*(vr + vl)*np.cos(theta + theta_dot3*self.del_t)
            y_dot4 = 0.5*(vr + vl)*np.sin(theta + theta_dot3*self.del_t)
            theta_dot4 = (vr - vl)/self.wb

            x_new = x + self.del_t/6.0*(x_dot1 + 2.0*x_dot2 + 2.0*x_dot3 + x_dot4)
            y_new = y + self.del_t/6.0*(y_dot1 + 2.0*y_dot2 + 2.0*y_dot3 + y_dot4)
            theta_new = theta + self.del_t/6.0*(theta_dot1 + 2.0*theta_dot2 + 2.0*theta_dot3 + theta_dot4)

            x_new = np.array([x_new, y_new, theta.new])

        return x_new
