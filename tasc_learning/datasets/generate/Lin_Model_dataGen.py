'''
This script generates datasets for the linear system
'''

import numpy as np
from scipy import signal
import random as rnd
import math
import pickle

# system parameters
n = 4 # number of robots
m_max = 1.5 # maximum robot mass
m_min = 0.5 # minimum robot mass
c_max = 2.0 # maximum robot damping coeff
c_min = 1.0 # minimum robot damping coeff
k_max = 2.0 # maximum robot spring coeff
k_min = 1.0 # minimum robot spring coeff
p_s_max = 10.0 # maximum robot initial positions
p_s_min = -10.0 # minimum robot initial positions
p_s_dot_max = 20.0 # maximum robot inital velocity
p_s_dot_min = -20.0 # minimum robot initial velocity
f_max = 50.0 # maximum force
f_min = -50.0 # minimum force
m_p = 10 # mass of parent
c_p = 1 # damping coeff for parent
k_p = 1 # spring coeff for parent
p_p_max = 10.0 # maximum inital position for parent
p_p_min = -10.0 # minimum initial postion for parent
p_p_dot_max = 20.0 # maximum initial velocity for parent
p_p_dot_min = -20.0 # minimum initial velocity for parent

# datagen parameters
run_t = 1.0 # time to run each simulation
n_time_step = 201 # number of timesteps per simulation
n_samples = 1000 # number of simulations to run
data_filename = '../../../data/linSys/set3_data.p' # where to save data
param_filename = '../../../data/linSys/set3_param.p' # where to save parameters

# generate swarm masses and damping coefficients
m_i = np.array(np.random.uniform(m_min, m_max, (1, n)))
c_i = np.array(np.random.uniform(c_min, c_max, (1, n)))
k_i = np.array(np.random.uniform(k_min, k_max, (1, n)))

# calculate matrices and set up systems
A_s = np.zeros((2*n, 2*n))
A_sp = np.zeros((2*n, 2))
B_s = np.zeros((2*n, n))

k_s = 0.0
c_s = 0.0

for i in range(n):
    A_s[2*i+0, 2*i+1] = 1
    A_s[2*i+1, 2*i+0] = -k_i[0, i]/m_i[0, i]
    A_s[2*i+1, 2*i+1] = -c_i[0, i]/m_i[0, i]

    A_sp[2*i+1, 0] = -k_i[0, i]/m_i[0, i]
    A_sp[2*i+1, 1] = -c_i[0, i]/m_i[0, i]

    B_s[2*i+1, i] = 1.0/m_i[0, i]

    k_s += k_i[0, i]
    c_s += c_i[0, i]

A_p = np.array([[0, 1], [-(k_p + k_s)/m_p, -(c_p + c_s)/m_p]])
B_p = np.array([[0, 0], [1.0/m_p, 1.0/m_p]])

A = np.zeros((2*n+2, 2*n+2))
B = np.zeros((2*n+2, n))

phi = np.zeros((2, 2*n))
phi[0, 0::2] = k_i
phi[1, 1::2] = c_i

A[0:2*n, 0:2*n] = A_s
A[0:2*n, 2*n:] = A_sp
A[2*n:, 0:2*n] = B_p@phi
A[2*n:, 2*n:] = A_p

B[0:2*n, :] = B_s

C = np.eye(2*n+2)
D = np.zeros((2*n+2, n))

sys = signal.lti(A, B, C, D)

# run simulations and generate datasets
data = []

for i in range(n_samples):
    f = np.array(np.random.uniform(f_min, f_max, n))
    p_s = np.array(np.random.uniform(p_s_min, p_s_max, n))
    p_s_dot = np.array(np.random.uniform(p_s_dot_min, p_s_dot_max, n))
    p_p = np.random.uniform(p_p_min, p_p_max)
    p_p_dot = np.random.uniform(p_p_dot_min, p_p_dot_max)

    x0 = np.zeros(2*n+2)
    x0[0:2*n:2] = p_s[:]
    x0[1:2*n:2] = p_s_dot[:]
    x0[2*n+0] = p_p
    x0[2*n+1] = p_p_dot

    t = np.linspace(0.0, run_t, n_time_step)
    u = np.ones((len(t), n))*f

    tout, yout, xout = signal.lsim(sys, u, t, x0)

    for j in range(n_time_step-1):
        data_entry = [xout[j, 0:2*n], xout[j, 2*n:], f,
                     xout[j+1, 0:2*n], xout[j+1, 2*n:]]
        data.append(data_entry)

n_train= len(data)

x_s_train = np.zeros((n_train, 2*n))
x_p_train = np.zeros((n_train, 2))
u_train = np.zeros((n_train, n))
x_sf_train = np.zeros((n_train, 2*n))
x_pf_train = np.zeros((n_train, 2))

for i in range(len(data)):
    x_s_train[i, :] = data[i][0]
    x_p_train[i, :] = data[i][1]
    u_train[i, :] = data[i][2]
    x_sf_train[i, :] = data[i][3]
    x_pf_train[i, :] = data[i][4]

train_dat = [x_s_train, x_p_train, u_train, x_sf_train, x_pf_train]

pickle.dump(train_dat, open(data_filename, 'wb'))
pickle.dump([n, A, B, C, D], open(param_filename, 'wb'))
