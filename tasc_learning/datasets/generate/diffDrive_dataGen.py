'''
This file generates datasets for a differential drive robot

command to generate dataset1:
python3 diffDrive_dataGen.py -n 10000 -d 0.01 -b 0.3 -x -3 -X 3 -y -3 -Y 3 -o ../../../data/diffDriveSys/datasets/diffDrive_dataset1_data.pkl -O ../../../data/diffDriveSys/datasets/diffDrive_dataset1_param.pkl
'''

import numpy as np
from scipy import integrate
from scipy import linalg
import getopt
import _pickle as pkl
import sys
import matplotlib.pyplot as plt
import matplotlib.animation as anim

class DiffDrive(object):

    def __init__(self, wb=1.0, x_range=(-10.0, 10.0), y_range=(-10.0, 10.0), u_range=(-1.0, 1.0)):
        self.setParams(wb)
        self.setRange(x_range, y_range, u_range)

    def setParams(self, wb=1.0):
        self.wb = wb

    def setRange(self, x_range=(-10.0, 10.0), y_range=(-10.0, 10.0), u_range=(-1.0, 1.0)):
        self.x_range = x_range
        self.y_range = y_range
        self.u_range = u_range

    def f(self, t, X, U):
        vl, vr = U
        x, y, theta = X

        x_dot = 0.5*(vr + vl)*np.cos(theta)
        y_dot = 0.5*(vr + vl)*np.sin(theta)
        theta_dot = (vr - vl)/self.wb

        return np.array([x_dot, y_dot, theta_dot])

    def jac(self, t, X, U):
        vr, vl = U
        x, y, theta = X

        J = np.zeros(3, 3)
        J[0, 2] = -0.5*(vr + vl)*sin(theta)
        J[1, 2] = 0.5*(vr + vl)*cos(theta)

        return J

    def genData(self, n, del_t):
        xmin = np.array([self.x_range[0], self.y_range[0], -np.pi]).reshape(3, 1)
        xmax = np.array([self.x_range[1], self.y_range[1],  np.pi]).reshape(3, 1)
        umin = np.array([self.u_range[0], self.u_range[0]]).reshape(2, 1)
        umax = np.array([self.u_range[1], self.u_range[1]]).reshape(2, 1)

        x0 = xmin.repeat(n, 1) + (xmax-xmin).repeat(n, 1)*np.random.rand(3, n)
        u = umin.repeat(n, 1) + (umax - umin).repeat(n, 1)*np.random.rand(n)
        xf = np.zeros((3, n))

        for i in range(n):
            r = integrate.ode(self.f, self.jac)
            r.set_integrator('dopri5')
            r.set_initial_value(x0[:, i], 0.0)
            r.set_f_params(u[:, i])
            r.integrate(del_t)
            xf[:, i] = r.y

        return [x0.T, u.T, xf.T]

    def genAnim(self, x0, u, del_t, output_file_path=None):
        n = u.shape[1]
        self.state = np.zeros((3, n))
        self.state[:, 0] = x0;

        r = integrate.ode(self.f)
        r.set_integrator('dopri5')
        r.set_initial_value(self.state[:, 0], 0.0)

        for i in range(1, n):
            r.set_f_params(u[:, i-1])
            r.integrate(r.t+del_t)
            self.state[:, i] = r.y
            #self.state[:, i] = self.state[:, i-1] + self.f(0, self.state[:, i-1], u[:, i-1])*del_t

        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111, aspect='equal', autoscale_on=False, xlim=self.x_range, ylim=self.y_range)
        self.ax.grid()
        self.line, = self.ax.plot([], [], lw=2)

        self.draw(0)

        self.an = anim.FuncAnimation(self.fig, self.draw, frames=n, interval=del_t*1000, blit=True)
        plt.show()

        if output_file_path != None:
            self.an.save(output_file_path, fps=1/del_t, extra_args=['-vcodec', 'libx264'])


    def draw(self, i):
        phi = np.linspace(0, 2*np.pi, 20)
        x = np.zeros(22)
        y = np.zeros(22)

        x[0] = self.state[0, i]
        y[0] = self.state[1, i]

        for j in range(20):
            x[j+1] = self.state[0, i] + self.wb*np.cos(phi[j] + self.state[2, i])
            y[j+1] = self.state[1, i] + self.wb*np.sin(phi[j] + self.state[2, i])

        x[21] = self.state[0, i] + self.wb*np.cos(self.state[2, i])
        y[21] = self.state[1, i] + self.wb*np.sin(self.state[2, i])

        self.line.set_data(x, y)

        return self.line,

def help():
    print('Usage: python diffDrive_dataGen.py [options]')
    print('-n [--nsamp]:     number of samples, default is 100')
    print('-h [--help]:      display this help')
    print('-d [--delt]:      time step to use, default is 0.001')
    print('-b [--wheelbase]: wheelbase of robot, default is 0.1')
    print('-x [--x_min]:     minimum value for initial x, default is -10')
    print('-X [--x_max]:     maximum value for initial x, default is 10')
    print('-y [--y_min]:     minimum value for initial y, default is -10')
    print('-Y [--y_max]:     maximum value for initial y, default is 10')
    print('-u [--u_min]:     minimum value for input, default is -1')
    print('-U [--u_max]:     maximum value for input, default is 1')
    print('-o [--out]:       output destination, default diffDrive_data.pkl')
    print('-O [--paramOut]:  output destination for params file, default diffDrive_param.pkl')
    print('-p [--plot]:      generate plot, use -o or --out to specify save file, shoule be .mp4.  default is not to save.')
    print('-P [--pltParams]: pickle file containing parameters for plotted simulation')

if __name__ == '__main__':
    n = 100
    del_t = 0.001
    wb = 0.1
    x_range = [-10, 10]
    y_range = [-10, 10]
    u_range = [-1, 1]
    file_data_out = 'diffDrive_data.pkl'
    file_param_out = 'diffDrive_param.pkl'
    plot = False
    plot_param_file = None

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'hn:d:b:x:X:y:Y:u:U:o:O:pP:', ['help', 'nsamp=', 'delt=', 'wheelbase=', 'x_min=', 'x_max=', 'y_min=', 'y_max=', 'u_min=', 'u_max=', 'out=', 'paramOut=', 'plot', 'pltParams='])
    except getopt.GetoptError as err:
        print('Error reading opts...')
        print(err.msg)
        help()
        sys.exit(2)

    for [opt, arg] in opts:
        if opt in ['-h', '--help']:
            help()
            sys.exit()
        elif opt in ['-n', '--nsamp']:
            n = int(arg)
        elif opt in ['-d', '--delt']:
            del_t = float(arg)
        elif opt in ['-b', '--wheelbase']:
            wb = float(arg)
        elif opt in ['-x', '--x_min']:
            x_range[0] = float(arg)
        elif opt in ['-X', '--x_max']:
            x_range[1] = float(arg)
        elif opt in ['-y', '--y_min']:
            y_range[0] = float(arg)
        elif opt in ['-Y', '--y_max']:
            y_range[1] = float(arg)
        elif opt in ['-u', '--u_min']:
            u_range[0] = float(arg)
        elif opt in ['-U', '--u_max']:
            u_range[1] = float(arg)
        elif opt in ['-o', '--out']:
            file_data_out = arg
        elif opt in ['-O', '--paramOut']:
            file_param_out = arg
        elif opt in ['-p', '--plot']:
            plot = True
            if file_data_out == 'diffDrive_data.pkl':
                file_data_out = None
        elif opt in ['-P', '--pltParams']:
            plot_param_file = arg

    mdl = DiffDrive(wb, x_range, y_range, u_range)

    if plot:
        if plot_param_file == None:
            print('no plot param file specified')
            sys.exit(1)
        else:
            x_0, u, del_t = pkl.load(open(plot_param_file, 'rb'))

            print('Plotting results for parameters from %s'%plot_param_file)
            print('wheelbase = %f'%wb)

            mdl.genAnim(x_0, u, del_t, file_data_out)
    else:
        print('Genering data for a Differential drive robot with a wheelbase of %f'%wb)
        print('data generated using intial x between %f and %f'%(x_range[0], x_range[1]))
        print('using initial y between %f and %f'%(y_range[0], y_range[1]))
        print(' and using inputs between %f and %f'%(u_range[0], u_range[1]))
        print('generating %d points with a time step of %f'%(n, del_t))
        data = mdl.genData(n, del_t)
        pkl.dump(data, open(file_data_out, 'wb'))
        pkl.dump([wb, del_t, x_range, y_range, u_range], open(file_param_out, 'wb'))

    print('Done!')
