'''
This Script generates data from a single pendulum

command used to generate last noninverted pendulum dataset:
python pend_dataGen.py -n 10000 -d 0.01 -t -3 -T 3 -o '../../../data/PendSys/datasets/pend_dataset2_data.pkl' -O '../../../data/pendSys/datasets/pend_dataset2_params.pkl'

command used to generate last inverted pendulum dataset
python3 pend_dataGen.py -n 1000000 -d 0.1 -t -6.2 -T 6.2 -I -o '../../../data/pendSys/datasets/invpend_dataset1_data.pkl' -O '../../../data/pendSys/datasets/invpend_dataset1_param.pkl'
'''

import numpy as np
from scipy import integrate
from scipy import linalg
import getopt
import _pickle as p
import sys
import matplotlib.pyplot as plt

class Pendulum_DataGen(object):
    m = None
    l = None
    g = None
    c = None
    theta_range = None
    omega_range = None
    u_range = None

    def __init__(self, m=1.0, l=1.0, g=9.81, c=1.0,
                 theta_range=(-2, 2), omega_range=(-1, 1),
                 u_range=(-10, 10), inv=False, lim_u=False, u_lim=1):
        self.setParams(m, l, g, c)
        self.setRange(theta_range, omega_range, u_range)
        self.inv=inv
        self.lim_u = lim_u
        self.u_lim = u_lim

    def setParams(self, m=1.0, l=1.0, g=9.81, c=1.0):
        self.m = m
        self.l = l
        self.g = g
        self.c = c

    def setRange(self, theta_range=(-2, 2), omega_range=(-1, 1),
                 u_range=(-10, 10)):
        self.theta_range = theta_range
        self.omega_range = omega_range
        self.u_range = u_range

    def f(self, t, x, u):
        theta = x[0]
        omega = x[1]

        if self.u_lim:
            u = self.u_lim*np.tanh(u)

        if self.inv:
            alpha = u/(self.m*self.l**2) + self.g/self.l*np.sin(theta) - self.c/(self.m*self.l**2)*omega
        else:
            alpha = u/(self.m*self.l**2) - self.g/self.l*np.sin(theta) - self.c/(self.m*self.l**2)*omega

        return np.array([omega, alpha])

    def jac(self, t, x):
        theta = x[0]

        if self.inv:
            return np.array([[0, 1], [self.g/self.l*np.cos(theta), -self.c/(self.m*self.l**2)]])
        else:
            return np.array([[0, 1], [-self.g/self.l*np.cos(theta), -self.c/(self.m*self.l**2)]])

    def genData(self, n, del_t):
        xmin = np.array([self.theta_range[0], self.omega_range[0]]).reshape(2, 1)
        xmax = np.array([self.theta_range[1], self.omega_range[1]]).reshape(2, 1)
        umin = self.u_range[0]
        umax = self.u_range[1]

        x0 = xmin.repeat(n, 1) + (xmax-xmin).repeat(n, 1)*np.random.rand(2, n)
        u = umin + (umax - umin)*np.random.rand(n)
        xf = np.zeros((2, n))

        for i in range(n):
            r = integrate.ode(self.f, self.jac)
            r.set_integrator('dopri5')
            r.set_initial_value(x0[:, i], 0.0)
            r.set_f_params(u[i])
            r.integrate(del_t)
            xf[:, i] = r.y

        return [x0.T, u, xf.T]

    def plot_res(self, n, t_f, x0, u):
        t = np.linspace(0.0, t_f, n)
        x = np.zeros((2, n))

        x[:, 0] = x0
        r = integrate.ode(self.f, self.jac)
        r.set_integrator('dopri5')
        r.set_initial_value(x0, 0.0)
        r.set_f_params(u)

        for i in range(1, n):
            r.integrate(t[i])
            x[:, i] = r.y

        plt.plot(t, x.T)
        plt.xlabel('time (s)')
        plt.ylabel('x')
        plt.show()

def help():
    print('Usage: python pend_dataGen.py [options]')
    print('-n [--nsamp]:     number of samples, default is 100')
    print('-h [--help]:      display this help')
    print('-d [--delt]:      time step to use, default is 0.001')
    print('-m [--mass]:      mass of object, default is 1.0')
    print('-g [--grav]:      spring constant, default is 9.81')
    print('-c [--damp]:      damping coefficient, default is 0.1')
    print('-l [--leng]:      length of arm, default is 1.0')
    print('-t [--theta_min]: minimum value for initial theta, default is -1')
    print('-T [--theta_max]: maximum value for initial theta, default is 1')
    print('-w [--omega_min]: minimum value for initial omega, default is -10')
    print('-W [--omega_max]: maximum value for initial omega, default is 10')
    print('-u [--u_min]:     minimum value for input, default is -10')
    print('-U [--u_max]:     maximum value for input, default is 10')
    print('-o [--out]:       output destination, default nonlin_data.pkl')
    print('-O [--paramOut]:  output destination for params file, default nonlin_param.pkl')
    print('-p [--plot]:      generate plot')
    print('-x [--x_init]:    initial x for plotted sim, default random')
    print('-i [--input]:     input for plotted sim, default random')
    print('-f [--final]:     final time for plotted simulation, default 3.0')
    print('-I [--inv]:       model with 0 as inverted confiburation')
    print('   [--lim_u]:     apply limits to the magnitude of u')
    print('   [--u_lim]:     the limit on the magnitude of u, default is 1')

def str2arr(string):
    rows = string.split(';')
    lst = []
    for i in range(len(rows)):
        lst.append(rows[i].split())

    return np.array(lst, dtype='float')

if __name__=='__main__':
    m = 1.0
    l = 1.0
    g = 9.81
    c = 0.1
    theta_range = [-1.0, 1.0]
    omega_range = [-10.0, 10.0]
    u_range = [-10.0, 10.0]
    x0 = None
    u = None
    t_f = 3.0
    plot = False
    inv = False
    lim_u = False
    u_lim = 1.0
    n = 100
    del_t = 0.001

    data_out = 'nonlin_data.pkl'
    param_out = 'nonlin_param.pkl'

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'hn:d:m:g:c:l:t:T:w:W:u:U:o:O:px:i:f:I', ['help', 'nsamp=', 'delt=', 'mass=', 'grav=', 'damp=', 'leng=', 'theta_min=', 'theta_max=', 'omega_min=', 'omega_max=', 'u_min=', 'u_max=', 'out=', 'paramOut=', 'plot', 'x_init=', 'input=', 'final=', 'inv', 'lim_u', 'u_lim='])
    except getopt.GetoptError as err:
        print('Error reading opts...')
        print(err.msg)
        help()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ['-h', '--help']:
            help()
            sys.exit()
        elif opt in ['-n', '--nsamp']:
            n = int(arg)
        elif opt in ['-d', '--delt']:
            del_t = float(arg)
        elif opt in ['-m', '--mass']:
            m = float(arg)
        elif opt in ['-g', '--grav']:
            g = float(arg)
        elif opt in ['-c', '--damp']:
            c = float(arg)
        elif opt in ['-l', '--damp']:
            l = float(arg)
        elif opt in ['-t', '--theta_min']:
            theta_range[0] = float(arg)
        elif opt in ['-T', '--theta_max']:
            theta_range[1] = float(arg)
        elif opt in ['-w', '--omega_min']:
            omega_range[0] = float(arg)
        elif opt in ['-W', '--omega_max']:
            omega_range[1] = float(arg)
        elif opt in ['-u', '--u_min']:
            u_range[0] = float(arg)
        elif opt in ['-U', '--u_max']:
            u_range[1] = float(arg)
        elif opt in ['-o', '--out']:
            data_out = arg
        elif opt in ['-O', '--paramOut']:
            param_out = arg
        elif opt in ['-p', '--plot']:
            plot = True
        elif opt in ['-x', '--x_init']:
            x0 = str2arr(arg).reshape(2)
        elif opt in ['-i', '--input']:
            u = float(arg)
        elif opt in ['-f', '--final']:
            t_f = float(arg)
        elif opt in ['-I', '--inv']:
            inv = True
        elif opt in ['--lim_u']:
            lim_u = True
        elif opt in ['--u_lim']:
            u_lim = float(arg)

    if plot and x0 is None:
        xmin = np.array([theta_range[0], omega_range[0]])
        xmax = np.array([theta_range[1], omega_range[1]])
        x0 = xmin + (xmax - xmin)*np.random.rand(2)

    if plot and u is None:
        umin = u_range[0]
        umax = u_range[1]
        u = umin + (umax - umin)*np.random.rand(1)

    if plot:
        print('Ploting sim of system with the following params:')
        print('m = %f'%m)
        print('l = %f'%l)
        print('c = %f'%c)
        print('b = %f'%g)
        print('With initial state [%f, %f] and input of %f'%(x0[0], x0[1], u))
        print('Sim will run for %fs'%t_f)
    else:
        print('Creating dataset for pendulum with the following params:')
        print('m = %f'%m)
        print('l = %f'%l)
        print('c = %f'%c)
        print('b = %f'%g)
        print('Generating %d datapoints with timestep %f where:'%(n, del_t))
        print('initial theta falls between %f and %f'%(theta_range[0], theta_range[1]))
        print('initial omega falls between %f and %f'%(omega_range[0], omega_range[1]))
        print('input u falls between %f and %f'%(u_range[0], u_range[1]))
        print('dataset being saved to: %s'%data_out)
        print('parameters being saved to: %s'%param_out)
        if lim_u:
            print('input is limited to +/-%f'%u_lim)
    if inv:
        print('System is modeled as inverted pendulum')

    print('generating...')

    gen = Pendulum_DataGen(m, l, g, c, theta_range, omega_range, u_range, inv, lim_u, u_lim)

    if plot:
        gen.plot_res(n, t_f, x0, u)
    else:
        data = gen.genData(n, del_t)
        p.dump(data, open(data_out, 'wb'))
        p.dump([m, l, c, g], open(param_out, 'wb'))

    print('Done!')
