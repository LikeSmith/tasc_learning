'''
This script generates datasets for a simple forced mass spring
damper system with an lqr.
'''

import numpy as np
from scipy import signal
from scipy import linalg
import getopt
import _pickle as p
import sys
import matplotlib.pyplot as plt

class LQR_DataGen(object):
    A = None
    B = None
    C = None
    D = None
    Q = None
    R = None

    def __init__ (self, m=1, k=0.1, c=0.1, Q=np.eye(2), R=np.eye(1)):
        self.A = np.array([[0.0, 1.0],  [-k/m, -c/m]])
        self.B = np.array([[0.0], [1.0/m]])
        self.Q = Q
        self.R = R

    def set_lqr(self, Q, R):
        self.Q = Q
        self.R = R

    def genData(self, n, del_t, x0, p=False):
        A_d = np.eye(2) + del_t*self.A
        B_d = del_t*self.B

        P = linalg.solve_discrete_are(A_d, B_d, self.Q, self.R)

        K = -linalg.inv(R + B_d.T@P@B_d)@(B_d.T@P@A_d)

        x = np.zeros((2, n+1))
        u = np.zeros((1, n))
        x[:, 0] = x0[:, 0]
        dataset = [[], [], []]

        for i in range(n):
            u[:, i] = K@x[:, i]
            x[:, i+1] = A_d@x[:, i] + B_d@u[:, i]

            dataset[0].append(x[:, i])
            dataset[1].append(u[:, i])
            dataset[2].append(x[:, i+1])

        if p:
            plt.plot(x.T)
            plt.show()

        dataset_ret = [[], [], []]
        dataset_ret[0] = np.array(dataset[0])
        dataset_ret[1] = np.array(dataset[1])
        dataset_ret[2] = np.array(dataset[2])

        return dataset_ret

    def genBabelData(self, n, del_t):
        A_d = np.eye(2) + del_t*self.A
        B_d = del_t*self.B

        x0 = np.random.rand(2, n)
        u = np.random.rand(1, n)

        xf = A_d@x0 + B_d@u

        return [x0.T, u.T, xf.T]

def help():
    print('Usage: python lqr_dataGen.py [options]')
    print('-n [--nsamp]:    number of samples, default is 100')
    print('-h [--help]:     display this help')
    print('-d [--delt]:     time step to use, default is 0.001')
    print('-m [--mass]:     mass of object, default is 1')
    print('-k [--spring]:   spring constant, default is 0.1')
    print('-c [--damp]:     damping coefficient, default is 0.1')
    print('-i [--x0]:       initial state, default is 1; 0')
    print('-Q [--Qmat]:     Q matrix, default is 1 0; 0 1')
    print('-R [--Rmat]:     R matrix, defualt is 1')
    print('-o [--out]:      output destination, default lqrdata.pkl')
    print('-O [--paramOut]: output destination for params file, default lqrparam.pkl')
    print('-p [--plot]:     plot results')
    print('-b [--babel]:    generate motor babel data')

def str2arr(string):
    rows = string.split(';')
    lst = []
    for i in range(len(rows)):
        lst.append(rows[i].split())

    return np.array(lst, dtype='float')

if __name__ == '__main__':
    n = 100
    del_t = 0.001
    m = 1
    k = 0.1
    c = 0.1
    x0 = np.array([[1], [0]])
    Q = np.eye(2)
    R = np.eye(1)
    out_file = 'lqrdata.pkl'
    param_file = 'lqrparam.pkl'
    plot = False
    babel = False

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'hn:d:m:k:c:i:Q:R:o:O:pb', ['help', 'nsamp=', 'delt=', 'mass=', 'spring=', 'damp=', 'x0=', 'Qmat=', 'Rmat=', 'out=', 'paramOut=', 'plot', 'babel'])
    except getopt.GetoptError as err:
        print('Error reading opts...')
        print(err.msg)
        help()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ['-h', '--help']:
            help()
            sys.exit()
        elif opt in ['-n', '--nsamp']:
            n = int(arg)
        elif opt in ['-d', '--delt']:
            del_t = float(arg)
        elif opt in ['-m', '--mass']:
            m = float(arg)
        elif opt in ['-k', '--spring']:
            k = float(arg)
        elif opt in ['-c', '--damp']:
            c = float(arg)
        elif opt in ['-i', '--x0']:
            x0 = str2arr(arg)
        elif opt in ['-Q', '--Qmat']:
            Q = str2arr(arg)
        elif opt in ['-R', '--Rmat']:
            R = str2arr(arg)
        elif opt in ['-o', '--out']:
            out_file = arg
        elif opt in ['-O', '--paramOut']:
            param_file = arg
        elif opt in ['-p', '--plot']:
            plot = True
        elif opt in ['-b', '--babel']:
            babel = True

    print('simulating %d steps, with timestep of %fs.'%(n, del_t))
    print('system has mass of %f, spring constatnt of %f, and damping coefficient of %f'%(m, k, c))
    if babel:
        print('generating babel data')
    else:
        print('x0=')
        print(x0)
        print('Q=')
        print(Q)
        print('R=')
        print(R)
    print('saving results to %s'%out_file)
    print('saving parameters to %s'%param_file)

    gen = LQR_DataGen(m, k, c, Q, R)

    if babel:
        dataset = gen.genBabelData(n, del_t)
    else:
        dataset = gen.genData(n, del_t, x0, plot)

    p.dump(dataset, open(out_file, 'wb'))
    p.dump([m, k, c, del_t, Q, R], open(param_file, 'wb'))

    print('Done')
