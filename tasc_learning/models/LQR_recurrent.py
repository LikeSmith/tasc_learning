'''
This class learns a finite horizon, discrete LQR Controller (in theory)
'''

import numpy as np
from keras.layers import Input, Dense
from keras.models import Model
from keras.callbacks import EarlyStopping
import keras.backend as K
import tasc_learning.models.lin_forward as f_model
import _pickle as pkl
import time

class LQR_recurrent(object):
    mdl = None
    horizon = -1
    n_states = -1
    n_inputs = -1

    def __init__(self,
                 n_states,
                 n_inputs,
                 Q,
                 R,
                 Q_f,
                 forward_model_path,
                 horizon=100,
                 variable_K=False,
                 from_params=False,
                 model_type=1):
        self.horizon = horizon
        self.n_states = n_states
        self.n_inputs = n_inputs
        self.Q = K.variable(Q)
        self.R = K.variable(R)
        self.Q_f = K.variable(Q_f)
        self.variable_K = variable_K

        self.setup_layers(from_params, forward_model_path)

        if model_type == 1:
            self.mdl = self.get_simp_feedback()
        elif model_type == 2:
            self.mdl = self.get_full_feedback()

    def setup_layers(self, from_params, forward_model_path):
        if from_params:
            [m, k, c, del_t, Q, R] = pkl.load(open(forward_model_path, 'rb'))
            A = np.array([[0.0, 1.0], [-k/m, -c/m]])
            B = np.array([[0.0],[1/m]])
            A_d = np.eye(2) + del_t*A
            B_d = del_t*B
            self.fwd_mdl = f_model.Lin_Forward(self.n_states,
                                               self.n_inputs)
            self.fwd_mdl.set_mats(A_d, B_d)
        else:
            self.fwd_mdl = f_model.Lin_Forward(self.n_states,
                                               self.n_inputs,
                                               from_file=True,
                                               file_path=forward_model_path)
        self.fwd_mdl.lock()

        self.x = []
        self.u = []

        self.x.append(Input(shape=(self.n_states,)))

        if self.variable_K:
            self.controller = []
        else:
            self.controller = Dense(self.n_inputs, name='LQR_Controller')

        for i in range(self.horizon):
            if self.variable_K:
                self.controller.append(Dense(self.n_inputs,
                                       name='LQR_Controller_%d'%i))
                self.u.append(self.controller[i](self.x[i]))
            else:
                self.u.append(self.controller(self.x[i]))

            self.x.append(self.fwd_mdl.mdl([self.x[i], self.u[i]]))

    def get_simp_feedback(self):
        mdl = Model(self.x[0], self.x[-1])
        mdl.compile(optimizer='rmsprop', loss=self.LQR_loss)
        return mdl

    def get_full_feedback(self):
        outputs = []
        losses = []

        for i in range(self.horizon):
            outputs.append(self.x[i])
            losses.append(self.state_loss)
            outputs.append(self.u[i])
            losses.append(self.ctrl_loss)

        outputs.append(self.x[-1])
        losses.append(self.final_loss)

        mdl = Model(self.x[0], outputs)
        mdl.compile(optimizer='rmsprop', loss=losses)
        return mdl

    def LQR_loss(self, x_f_des, x_f_act):
        err_f = x_f_act - x_f_des
        loss = self.quad(err_f, self.Q_f)
        #loss = 0.0

        for i in range(self.horizon):
            err_i = self.x[i] - x_f_des
            loss += self.quad(err_i, self.Q)
            loss += self.quad(self.u[i], self.R)

        return loss

    def state_loss(self, x_des, x_act):
        err = x_act - x_des
        return self.quad(err, self.Q)

    def final_loss(self, x_des, x_act):
        err = x_act - x_des
        return self.quad(err, self.Q_f)

    def ctrl_loss(self, u_des, u_act):
        return self.quad(u_act, self.R)

    def quad(self, x, A):
        a = K.dot(A, K.transpose(x))
        return K.dot(x, a)

    def train(self, x_0, x_f, val_split=0.2, epochs=50,
              batch_size=10, verbosity=2, patience=1):
        es = EarlyStopping(monitor='val_loss', patience=patience)
        hist = self.mdl.fit(x_0, x_f,
                            batch_size=batch_size,
                            epochs=epochs,
                            validation_split=val_split,
                            verbose=verbosity,
                            callbacks=[es])
        return hist

    def get_controller(self):
        if self.variable_K:
            ctrl = []
            for i in range(self.horizon):
                x_i = Input(shape=(self.n_states,))
                u = self.controller[i](x_i)
                ctrl.append(Model(x_i, u))
                ctrl[i].compile(optimizer='rmsprop', loss='mse')
        else:
            x_i = Input(shape=(self.n_states,))
            u = self.controller(x_i)
            ctrl = Model(x_i, u)
            ctrl.compile(optimizer='rmsprop', loss='mse')

        return ctrl

    def save_controller(self, save_path):
        if self.variable_K:
            ctrl = self.get_controller()
            for i in range(self.horizon):
                ctrl[i].save(save_path%i)
        else:
            self.get_controller().save(save_path)

    def set_controller(self, K):
        if self.variable_K:
            for i in range(self.horizon):
                self.controller[i].set_weights(K[i, :])
        else:
            self.controller.set_weights([K.reshape(self.n_states, self.n_inputs), np.array([0.0])])

    def save(self, save_path):
        self.mdl.save(save_path)

    def save_forward_model(self, save_path):
        self.fwd_mdl.save(save_path)

    def get_full_output(self, n, x_des=np.array([0.0, 0.0])):
        outputs = []
        u_des_arr = np.zeros((n, 1))
        x_des_arr = np.repeat(x_des.reshape(1, self.n_states), n, axis=0)

        for i in range(self.horizon):
            outputs.append(x_des_arr)
            outputs.append(u_des_arr)

        outputs.append(x_des_arr)
        return outputs
