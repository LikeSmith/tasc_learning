import os.path as op

import numpy as np

import keras.backend as K
from keras.layers import Input, Dense, merge
from keras.models import Model, load_model
from keras.objectives import mse
from keras.callbacks import EarlyStopping


class forward_dynamics(object):
    def __init__(self,
                 model_path,
                 state_inputs, control_inputs, output_shape,
                 n_states=0, n_controls=0,
                 layer_dims=[0],
                 load_file=True, merge_mode='concat'
                 ):
        # Is there a better way to get the number of states and
        # controls when loading a model? Ideally these values would
        # not need to be parameters.

        if load_file:
            assert(op.exists(model_path))
            assert(n_states > 0)
            assert(n_controls > 0)
            self.n_states = n_states
            self.n_controls = n_controls
            self.model = load_model(model_path)
            self.model.compile(
                optimizer='rmsprop',
                loss={'output': mse}
            )
                
        else:
            if not isinstance(state_inputs, list):
                state_inputs = [state_inputs]
            self.state_inputs = state_inputs
            self.n_states = len(state_inputs)
            
            if not isinstance(control_inputs, list):
                control_inputs = [control_inputs]
            self.control_inputs = control_inputs
            self.n_controls = len(control_inputs)

            self.model = self.build(
                inputs=state_inputs + control_inputs,
                output_shape=output_shape,
                layer_dims=layer_dims,
                merge_mode=merge_mode
            )

            self.model.compile(
                optimizer='rmsprop',
                loss={'output': mse}
            )

            self.model.save(model_path)

    def build(self,
              inputs, output_shape,
              layer_dims,
              merge_mode='concat'
    ):

        if not isinstance(layer_dims, list):
            layer_dims = [layer_dims]

        for layer_dim in layer_dims:
            assert(isinstance(layer_dim, int))
            assert(layer_dim > 0)

        next_inputs = merge(
            inputs,
            mode=merge_mode
        )            

        for idx in np.arange(len(layer_dims)):
            h = Dense(
                layer_dims[idx],
                activation='relu'
            )(next_inputs)
            next_inputs = h

        self.output = Dense(
            output_shape,
            activation='softmax',
            name='output'
        )(next_inputs)

        return Model(inputs, [self.output])


class policy_trainer(object):
    def __init__(self, forward_dynamics, output_shape, control_shape):
        self.fd = fd = forward_dynamics

        zero_control = K.variable(
            np.zeros((1, control_shape)),
            name='zero_control'
        )
        h = fd.model(fd.model.inputs)
        h = merge(
            [h, fd.model.inputs[1]], # Is there a better way to do
            # this? This is specific to the current dataset.
            mode='concat',
            name='policy_network_input'
        )
        h = Dense(100)(h) # TODO add layer_dims parameter
        h = Dense(100)(h)
        h = Dense(control_shape, name='policy_network_output')(h)

        state_inputs = fd.model.inputs[
            :-fd.n_controls
        ]
        self.output = fd.model(
            state_inputs + [h]
        )

        outputs = [self.output, h]
        self.model = Model(
            fd.model.inputs,
            [self.output, h]
        )
                    
        self.model.compile(
            optimizer='rmsprop',
            loss=mse
        )


if __name__ == '__main__':
    from tasc_learning.datasets.LinSys import LinSys
    data = LinSys()
    
    parent_state_shape = data.xp_t.shape[1]
    swarm_state_shape = data.xs_t.shape[1]
    control_shape = data.u_t.shape[1]
    
    parent_input = Input(
        shape=(parent_state_shape,),
        name='parent_state_input'
    )
    swarm_input = Input(
        shape=(swarm_state_shape,),
        name='swarm_state_input'
    )
    control_input = Input(
        shape=(control_shape,),
        name='control_state'
    )
    state_inputs = [parent_input, swarm_input]

    fd = forward_dynamics(
        'fd_test.h5',
        state_inputs, control_input,
        output_shape=parent_state_shape,
        n_states = 2,
        n_controls = 1,
        layer_dims=100,
        load_file=False
    )

    X_train = [data.xp_t, data.xs_t, data.u_t]
    Y_train = [data.xp_t1]
    fd.model.fit(
        X_train, Y_train,
        batch_size=32,
        nb_epoch=1000,
        validation_split=0.2,
        callbacks=[EarlyStopping(patience=10)]
    )

    Y_train += [data.u_t]
    pt = policy_trainer(fd, parent_state_shape, control_shape)
    pt.model.fit(
        X_train, Y_train,
        batch_size=32,
        nb_epoch=1000,
        validation_split=0.2,
        callbacks=[EarlyStopping(patience=10)]
    )
