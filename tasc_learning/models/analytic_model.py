'''
untrainable model for adding analyic first order differential equation
to the network
'''

import numpy as np
from keras.layers import Input, Lambda, Concatenate
from keras.models import Model
from keras.callbacks import EarlyStopping
from keras.layers import Lambda
import keras.backend as K

class Analytic_Model(object):
    def __init__(self, n_states, n_inputs, diff_funcs, method='RK1', del_t=0.1):
        self.n_states = n_states
        self.n_inputs = n_inputs
        self.cont_funcs = diff_funcs
        self.del_t = del_t

        self.generate_disc_funcs(method)

        self.x_in = Input(shape=(n_states,))
        self.u_in = Input(shape=(n_inputs,))

        self.lambdas = []

        for i in range(self.n_states):
            self.lambdas.append(Lambda(self.disc_funcs[i], output_shape=(1,))([self.x_in, self.u_in]))
        #import pdb; pdb.set_trace()
        self.x_out = Concatenate(axis=-1)(self.lambdas)

        self.mdl = Model([self.x_in, self.u_in], self.x_out)
        #self.mdl.compile(optimizer='rmsprop', loss='mse')

    def generate_disc_funcs(self, method):
        self.disc_funcs = []
        for i in range(self.n_states):
            if method == 'RK1':
                self.disc_funcs.append(lambda args: self.RK1(args, self.cont_funcs[i], i))

    def RK1(self, args, f, i):
        x, u = args
        x_t1 = x[:, i] + self.del_t*f(x, u).T
        return x_t1

class Analytic_Model_single(object):
    def __init__(self, n_states, n_inputs, diff_func):
        self.n_states = n_states
        self.n_inputs = n_inputs

        self.x_in = Input(shape=(self.n_states,))
        self.u_in = Input(shape=(self.n_inputs,))
        self.x_out = Lambda(diff_func, output_shape=(self.n_states,))([self.x_in, self.u_in])

        self.mdl = Model([self.x_in, self.u_in], self.x_out)
