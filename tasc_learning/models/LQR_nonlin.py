'''
stacked LQR model that learns on nonlinear feedback control law
for a nonlinear system that minimizes the LQR cost function
'''

import numpy as np
from keras.layers import Input, Lambda
from keras.models import Model
from keras.callbacks import EarlyStopping
import keras.backend as K
import tasc_learning.models.nonlin_forward as f_model
import tasc_learning.models.analytic_model as a_model
import tasc_learning.models.mlp as mlp
import _pickle as pkl

class LQR_Nonlin(object):
    def __init__(self, n_states, n_inputs, forward_model_path,
                 n_hidden_states_forward, n_hidden_states_ctrl,
                 Q, R, Q_f, horizon=100, from_dynamics=False, f=None,
                 del_t=0.1):
        self.n_states = n_states
        self.n_inputs = n_inputs
        self.n_hidden_states_forward = n_hidden_states_forward
        self.n_hidden_states_ctrl = n_hidden_states_ctrl

        self.Q = K.variable(Q)
        self.R = K.variable(R)
        self.Q_f = K.variable(Q_f)

        self.horizon = horizon

        if from_dynamics:
            self.fwd_mdl = a_model.Analytic_Model_single(self.n_states, self.n_inputs, f)
        else:
            self.fwd_mdl = f_model.Nonlin_Forward(self.n_states,
                                                  self.n_inputs,
                                                  self.n_hidden_states_forward,
                                                  from_file=True,
                                                  file_path=forward_model_path)
            self.fwd_mdl.lock()
        
        activ_funcs = []
        for i in range(len(self.n_hidden_states_ctrl)):
            activ_funcs.append('relu')
            
        self.ctrl = mlp.MLP(self.n_states, self.n_inputs, 1,
                            self.n_hidden_states_ctrl, activ_funcs,
                            name='controller')

        self.x = [Input(shape=(self.n_states,))]
        self.u = []

        for i in range(self.horizon):
            self.u.append(self.ctrl.mdl(self.x[i]))
            self.x.append(self.fwd_mdl.mdl([self.x[i], self.u[i]]))

        self.mdl = Model(self.x[0], self.x[-1])
        self.mdl.compile(optimizer='adam', loss=self.LQR_loss)

    def LQR_loss(self, x_f_des, x_f_act):
        err_f = x_f_act - x_f_des
        loss = self.quad(err_f, self.Q_f)
        #loss = 0.0

        for i in range(self.horizon):
            err_i = self.x[i] - x_f_des
            loss += self.quad(err_i, self.Q)
            loss += self.quad(self.u[i], self.R)

        return loss

    def quad(self, x, A):
        a = K.dot(A, K.transpose(x))
        return K.dot(x, a)

    def train(self, x_0, x_f, val_split=0.2, epochs=50,
              batch_size=10, verbosity=2, patience=1):
        es = EarlyStopping(monitor='val_loss', patience=patience)
        hist = self.mdl.fit(x_0, x_f,
                            batch_size=batch_size,
                            epochs=epochs,
                            validation_split=val_split,
                            verbose=verbosity,
                            callbacks=[es])
        return hist

    def get_controller(self):
        x = Input(shape=(self.n_states,))
        u = self.ctrl(x)
        return Model(x, u)

    def save_controller(self, save_path):
        self.ctrl.save(save_path)

    def save(self, save_path):
        self.mdl.save(save_path)
