'''
class for creating a MultiLayer Perceptron (MLP)
'''

import numpy as np

from keras.layers import Input, Dense
from keras.models import Model, load_model
from keras.callbacks import EarlyStopping

class MLP(object):

    def __init__(self, input_size, output_size, n_hidden,
                 hidden_size, hidden_activation,
                 output_activation='linear', name='mlp',
                 from_file=False, file_path='mlp.h5'):
        self.input_size = input_size
        self.output_size = output_size
        self.n_hidden = n_hidden
        self.hidden_size = hidden_size
        self.hidden_activation = hidden_activation
        self.output_activation = output_activation
        self.name = name

        if from_file:
            self.mdl = load_model(file_path)
            self.mdl.compile(optimizer='rmsprop', loss='mse')
        else:
            self.input = Input(shape=(self.input_size,),
                               name=self.name+'_input')
            self.hidden_l = []
            self.hidden = []
            for i in range(len(hidden_size)):
                self.hidden_l.append(Dense(hidden_size[i],
                                           activation=hidden_activation[i],
                                           name=self.name+'_hidden%d'%i))
                if i == 0:
                    self.hidden.append(self.hidden_l[i](self.input))
                else:
                    self.hidden.append(self.hidden_l[i](self.hidden[i-1]))

            self.output_l = Dense(self.output_size,
                                activation=self.output_activation,
                                name=self.name+'_output')
            self.output = self.output_l(self.hidden[-1])

            self.mdl = Model(self.input, self.output)
            self.mdl.compile(optimizer='rmsprop', loss='mse')

    def train(self, train_in, train_out, val_split=0.2, epochs=50,
              batch_size=10, verbosity=2, patience=1):
        es = EarlyStopping(monitor='val_loss', patience=patience)
        hist = self.mdl.fit(train_in, train_out,
                            batch_size=batch_size,
                            epochs=epochs,
                            validation_split=val_split,
                            verbose=verbosity,
                            callbacks=[es])
        return hist

    def lock(self):
        lockable_layers = [self.name+'_output']
        for i in range(self.n_hidden):
            lockable_layers.append(self.name+'_hidden%d'%i)

        for layer in self.mdl.layers:
            if layer.name in lockable_layers:
                layer.trainable = False

    def unlock(self):
        lockable_layers = [self.name+'_output']
        for i in range(self.n_hidden):
            lockable_layers.append(self.name+'_hidden%d'%i)

        for layer in self.mdl.layers:
            if layer.name in lockable_layers:
                layer.trainable = True

    def eval(self, x):
        batch_size = int(x.size/2)
        return self.mdl.predict(x.reshape(batch_size, 2))

    def save(self, save_path):
        self.mdl.save(save_path)
