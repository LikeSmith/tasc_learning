'''
This class learns an LQR controller by demonstration
'''

from keras.layers import Input, Dense
from keras.models import Model
from keras.callbacks import EarlyStopping

class LQR_basic(object):
    mdl = None

    def __init__(self, n_in=2, n_out=1, batch_size=10):
        self.batch_size = batch_size

        x_t = Input(batch_shape=(batch_size, n_in))
        u = Dense(n_out)(x_t)

        self.mdl = Model(x_t, u)
        self.mdl.compile(optimizer='rmsprop',
                         loss='mse')

    def train(self, train_in, train_out, nb_epoch, val_split, verbosity=2):
            hist = self.mdl.fit(train_in, train_out,
                           batch_size=self.batch_size,
                           nb_epoch=nb_epoch,
                           validation_split=val_split,
                           #callbacks=[EarlyStopping(patience=1)],
                           verbose=verbosity)
            return hist

    def get_model(self):
        return self.mdl
