'''
This class learns a discrete modle of a linear system based on motor
bable data
'''

import numpy as np
from keras.layers import Input, Dense, Concatenate
from keras.models import Model, load_model
from keras.callbacks import EarlyStopping

class Lin_Forward(object):
    mdl = None

    def __init__(self, n_states, n_controls, name='model',
                 from_file=False, file_path='model.h5'):
        self.n_states = n_states
        self.n_controls = n_controls
        self.name = name

        if from_file:
            self.mdl = load_model(file_path)
            self.mdl.compile(optimizer='rmsprop',
                             loss='mse')
        else:
            self.x_t = Input(shape=(self.n_states,),
                             name=self.name+'_x_t')
            self.u_t = Input(shape=(self.n_controls,),
                             name=self.name+'_u_t')

            self.m = Concatenate(name=self.name+'_m')([self.x_t, self.u_t])
            self.x_t1 = Dense(self.n_states,
                              use_bias=False,
                              name=self.name+'_x_t1')(self.m)

            self.mdl = Model([self.x_t, self.u_t], self.x_t1)
            self.mdl.compile(optimizer='rmsprop',
                             loss='mse')

    def set_mats(self, A, B):
        w = np.concatenate([A, B], axis=1)
        self.mdl.layers[3].set_weights([w.T, np.zeros(2)])

    def train(self, train_in, train_out, val_split=0.2, epochs=50,
              batch_size=10, verbosity=2, patience=1):
        es = EarlyStopping(monitor='val_loss', patience=patience)
        hist = self.mdl.fit(train_in, train_out,
                            batch_size=batch_size,
                            epochs=epochs,
                            validation_split=val_split,
                            verbose=verbosity,
                            callbacks=[es])
        return hist

    def lock(self):
        for i in range(len(self.mdl.layers)):
            if self.mdl.layers[i].name == self.name+'_x_t1':
                self.mdl.layers[i].trainable = False

        self.mdl.compile(optimizer='rmsprop',
                         loss='mse')

    def unlock(self):
        for i in range(len(self.mdl.layers)):
            if self.mdl.layers[i].name == self.name+'_x_t1':
                self.mdl.layers[i].trainable = False

        self.mdl.compile(optimizer='rmsprop',
                         loss='mse')

    def save(self, save_path):
        self.mdl.save(save_path)
