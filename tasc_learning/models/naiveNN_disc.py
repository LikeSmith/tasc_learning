'''
This file contains a class that creates and trains a Neural
Network that takes a naive approach to learning the parent-
swarm style system
'''

from keras.layers import Input, Dense, merge, Dropout
from keras.models import Model
from keras.callbacks import EarlyStopping

class naiveNN(object):
    mdl = None
    xs_t_h_l = None
    abs_state_l = None
    u_h_l = None
    u_t1_l = None
    batch_size = 10
    sys_params = None
    hyper_params = None

    def __init__(self, sys_params, hyper_params, batch_size=10):
        n_swarm_states, n_swarm_inputs, n_parent_states = sys_params
        n_abs_states, n_xs_hidden, n_parent_hidden, n_swarm_hidden, n_u_hidden, dropout_rate = hyper_params
        self.batch_size = batch_size
        self.sys_params = sys_params
        self.hyper_params = hyper_params

        # wire network
        # setup inputs
        xs_t = Input(batch_shape=(batch_size, n_swarm_states))
        xp_t = Input(batch_shape=(batch_size, n_parent_states))
        u_t = Input(batch_shape=(batch_size, n_swarm_inputs))

        # calculate abstract state
        self.xs_t_h_l = Dense(n_xs_hidden, activation='tanh')
        self.abs_state_l = Dense(n_abs_states)
        xs_t_h = self.xs_t_h_l(xs_t)
        xs_t_h_d = Dropout(dropout_rate)(xs_t_h)
        abs_state = self.abs_state_l(xs_t_h_d)

        # calculate new parent state
        parent_merge = merge([abs_state, xp_t], mode='concat')
        parent_merge_h = Dense(n_parent_hidden, activation='tanh')(parent_merge)
        parent_merge_h_d = Dropout(dropout_rate)(parent_merge_h)
        xp_t1 = Dense(n_parent_states)(parent_merge_h_d)

        # calculate new swarm state
        swarm_merge = merge([xs_t, xp_t, u_t], mode='concat')
        swarm_merge_h = Dense(n_swarm_hidden, activation='tanh')(swarm_merge)
        swarm_merge_h_d = Dropout(dropout_rate)(swarm_merge_h)
        xs_t1 = Dense(n_swarm_states)(swarm_merge_h_d)

        # calculate control input
        u_merge = merge([abs_state, xp_t, xp_t1], mode='concat')
        self.u_h_l = Dense(n_u_hidden, activation='tanh')
        self.u_t1_l = Dense(n_swarm_inputs)
        u_h = self.u_h_l(u_merge)
        u_h_d = Dropout(dropout_rate)(u_h)
        u_t1 = self.u_t1_l(u_h_d)

        # compile model
        self.mdl = Model([xs_t, xp_t, u_t], [xp_t1, xs_t1, u_t1])
        self.mdl.compile(optimizer='rmsprop',
                         loss=['mse', 'mse', 'mse'],
                         loss_weights=[1, 1, 10])

    def train(self, train_in, train_out, nb_epoch, val_split, verbosity=2):
            hist = self.mdl.fit(train_in, train_out,
                           batch_size=self.batch_size,
                           nb_epoch=nb_epoch,
                           validation_split=val_split,
                           callbacks=[EarlyStopping(patience=1)],
                           verbose=verbosity)
            return hist

    def get_abstractor(self):
        n_swarm_states, n_swarm_inputs, n_parent_states = self.sys_params
        n_abs_states, n_xs_hidden, n_parent_hidden, n_swarm_hidden, n_u_hidden, dropout_rate = self.hyper_params

        xs_t_in = Input(shape=(n_swarm_states,))

        xs_t_h2 = self.xs_t_h_l(xs_t_in)
        abs_state2 = self.abs_state_l(xs_t_h2)

        abstractor = Model(xs_t_in, abs_state2)

        return abstractor

    def get_controller(self):
        n_swarm_states, n_swarm_inputs, n_parent_states = self.sys_params
        n_abs_states, n_xs_hidden, n_parent_hidden, n_swarm_hidden, n_u_hidden, dropout_rate = self.hyper_params
        
        abs_state_in = Input(shape=(n_abs_states,))
        xp_t1_in = Input(shape=(n_parent_states,))
        xp_t_in = Input(shape=(n_parent_states,))

        u_merge2 = merge([abs_state_in, xp_t_in, xp_t1_in], mode='concat')
        u_h2 = self.u_h_l(u_merge2)
        u_t12 = self.u_t1_l(u_h2)

        controller = Model([abs_state_in, xp_t_in, xp_t1_in], u_t12)

        return controller

    def get_full_model(self):
        return self.mdl
