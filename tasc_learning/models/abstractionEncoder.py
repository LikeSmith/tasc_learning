'''
This Neural Network is designed to learn an abstraction of a swarm state
in a parent, swarm system.
'''

import numpy as np
import os
from keras.layers import Input, Concatenate
from keras.models import Model, load_model
from keras.callbacks import EarlyStopping
import tasc_learning.models.mlp as mlp
import _pickle as pkl

class AbstractionEncoder(object):
    home = os.path.dirname(os.path.realpath(__file__)) + '/../../data/results/'

    def __init__(self, parent_state_size, swarm_state_size, abs_state_size, abs_enc_layers=[10], abs_enc_activ=['tanh'], abs_enc_activ_end='linear', abs_dyn_layers=[10], abs_dyn_activ=['tanh'], abs_dyn_activ_end='linear', from_file=False, abs_enc_file='abs_enc.h5', abs_enc_params='abs_enc_params.pkl', abs_cyn_file='abs_dyn.h5', abs_dyn_params='abs_dyn_params.pkl', name='abstraction_encoder', home = None):

        self.parent_state_size = parent_state_size
        self.swarm_state_size = swarm_state_size
        self.abs_state_size = abs_state_size
        self.name = name

        if from_file:
            if home != None:
                self.home = home
            self.abs_enc_layers, self.abs_enc_activ, self.abs_enc_activ_end = pkl.load(open(self.home+abs_enc_params, 'rb'))
            self.abs_dyn_layers, self.abs_dyn_activ, self.abs_enc_activ_end = pkl.load(open(self.home+abs_dyn_params, 'rb'))

            self.abs_enc = mlp(input_size=swarm_state_size, output_size=abs_State_size, n_hidden=len(self.abs_enc_layers), hidden_size=self.abs_enc_layers, hidden_activation=self.abs_enc_activ, output_activation=self.abs_enc_activ_end, name=self.name+'abs_enc', from_file=True, file_path=self.home+abs_enc_file)
            self.abs_dyn = mlp(input_size=abs_state_size+parent_state_size, output_size=parent_state_size, n_hidden=len(self.abs_dyn_layers), hidden_size=self.abs_dyn_layers, hidden_activation=self.abs_dyn_activ, output_activation=self.abs_dyn_activ_end, name=self.name+'abs_dyn', from_file=True, file_path=self.home+abs_dyn_file)
        else:
            self.abs_enc_layers = abs_enc_layers
            self.abs_enc_activ = abs_enc_activ
            self.abs_enc_activ_end = abs_enc_activ_end
            self.abs_dyn_layers = abs_dyn_layers
            self.abs_dyn_activ = abs_dyn_activ
            self.abs_dyn_activ_end = abs_dyn_activ_end

            self.abs_enc = mlp.MLP(input_size=swarm_state_size, output_size=abs_state_size, n_hidden=len(self.abs_enc_layers), hidden_size=self.abs_enc_layers, hidden_activation=self.abs_enc_activ, output_activation=self.abs_enc_activ_end, name=self.name+'_abs_enc')
            self.abs_dyn = mlp.MLP(input_size=abs_state_size+parent_state_size, output_size=parent_state_size, n_hidden=len(self.abs_dyn_layers), hidden_size=self.abs_dyn_layers, hidden_activation=self.abs_dyn_activ, output_activation=self.abs_dyn_activ_end, name=self.name+'_abs_dyn')

        self.xp_0 = Input(shape=(self.parent_state_size,), name=self.name+'_xp_0')
        self.xs_0 = Input(shape=(self.swarm_state_size,), name=self.name+'_xs_0')

        self.abs = self.abs_enc.mdl(self.xs_0)
        self.dyn_in = Concatenate(name=self.name+'_concat')([self.xp_0, self.abs])
        self.xp_t = self.abs_dyn.mdl(self.dyn_in)

        self.mdl = Model([self.xp_0, self.xs_0], self.xp_t)
        self.mdl.compile(optimizer='RMSprop', loss='mse')

    def train(self, xp_0, xs_0, xp_t, val_split=0.2, epochs=1000, batch_size=10, verbosity=2, patience=100):
        es = EarlyStopping(monitor='val_loss', patience=patience)
        hist = self.mdl.fit([xp_0, xs_0], xp_t, batch_size=batch_size, epochs=epochs, validation_split=val_split, verbose=verbosity, callbacks=[es])
        return hist

    def get_abs_enc(self):
        return self.abs_enc

    def get_abs_dyn(self):
        return self.abs_dyn

    def save(self, abs_enc_name, abs_enc_params, abs_dyn_name, abs_dyn_params):
        self.abs_enc.save(self.home+abs_enc_name)
        self.abs_dyn.save(self.home+abs_dyn_name)
        pkl.dump([self.abs_enc_layers, self.abs_enc_activ, self.abs_enc_activ_end], open(self.home+abs_enc_params, 'wb'))
        pkl.dump([self.abs_dyn_layers, self.abs_dyn_activ, self.abs_dyn_activ_end], open(self.home+abs_dyn_params, 'wb'))

    def set_home(self, new_home):
        self.home = new_home
