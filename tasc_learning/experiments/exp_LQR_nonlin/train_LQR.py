'''
Train the recursive nonlinear LQR controller
'''

import numpy as np
import os
import tasc_learning.models.LQR_nonlin as NN
import _pickle as pkl

if __name__=='__main__':
    results_dir = '../../../data/pendSys/results/'
    fwd_model_name = 'nonlin_forward_mdl1.h5'
    model_params_name = '../datasets/pend_dataset2_param.pkl'
    model_name = 'LQR_nonlin_mdl1.h5'
    ctrlr_name = 'LQR_controller_mdl1.h5'
    hist_name = 'LQR_recurrent_hist1.pkl'
    os.makedirs(results_dir, exist_ok=True)

    Q = np.array([[10.0, 0.0], [0.0, 1.0]])*0.1
    Q_f = Q
    R = np.eye(1)*0.1*0.1
    horizon = 20
    batch_size = 10
    n_hidden_states_forward = 30
    n_hidden_states_ctrl = 10

    model = NN.LQR_Nonlin(2, 1, results_dir+fwd_model_name,
                          n_hidden_states_forward, n_hidden_states_ctrl,
                          Q, R, Q_f, horizon)

    x_0 = np.random.rand(10000, 2)*np.array([6.2832, 20]) - np.array([3.1416, 10])
    x_f = np.zeros((10000, 2))

    model.train(x_0, x_f, 0.2, 100, batch_size, 1, 1)

    model.save(results_dir+model_name)
    model.save_controller(results_dir+ctrlr_name)
