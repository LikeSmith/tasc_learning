'''
Tests learned LQR model
'''

import numpy as np
import tasc_learning.models.mlp as NN
import matplotlib.pyplot as plt
from scipy import integrate
import _pickle as pkl

def f(t, x, params):
    m, l, c, g, Q, R, ctrlr = params
    theta = x[0]
    omega = x[1]
    u = ctrlr.eval(x[0:2])
    alpha = u/(m*l**2) - g/l*np.sin(theta) - c/(m*l**2)*omega
    L = theta**2*Q[0, 0] + omega**2*Q[1, 1] + 2*theta*omega*Q[0, 1] + u**2*R[0, 0]
    return np.array([omega, alpha, L])

if __name__ == '__main__':
    results_dir = '../../../data/pendSys/results/'
    model_params_name = '../datasets/pend_dataset2_param.pkl'
    ctrlr_name = 'LQR_controller_mdl1.h5'

    t_f = 4.0
    n = 100
    x0 = np.array([1.0, 1.0, 0.0])
    Q = np.array([[10.0, 0.0], [0.0, 1.0]])
    R = np.eye(1)*0.1

    m, l, c, g = pkl.load(open(results_dir+model_params_name, 'rb'))

    ctrlr = NN.MLP(2, 1, 1, [10], ['tanh'], from_file=True,
                   file_path=results_dir+ctrlr_name)

    t = np.linspace(0.0, t_f, n)
    u = np.zeros(n)
    x = np.zeros((3, n))

    x[:, 0] = x0

    r = integrate.ode(f)
    r.set_integrator('dopri5')
    r.set_initial_value(x0, 0.0)
    r.set_f_params([m, l, c, g, Q, R, ctrlr])

    for i in range(1, n):
        r.integrate(t[i])
        x[:, i] = r.y
        u[i] = ctrlr.eval(x[0:2, i-1])

    plt.figure()
    plt.plot(t, x[0:2, :].T)
    plt.xlabel('time (s)')
    plt.ylabel('x')
    plt.figure()
    plt.plot(t, x[2, :])
    plt.xlabel('time (s)')
    plt.ylabel('J')
    plt.figure()
    plt.plot(t, u)
    plt.xlabel('time (s)')
    plt.ylabel('u')
    plt.show()
