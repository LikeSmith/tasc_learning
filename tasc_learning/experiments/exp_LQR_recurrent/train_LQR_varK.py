'''
This file trains the LQR controller
'''

import numpy as np
import os
import tasc_learning.models.LQR_recurrent as NN
import _pickle as pkl

if __name__ == '__main__':
    results_dir = '../../../data/LQRSys/results/'
    fwd_model_name = 'LQR_forward_mdl1.h5'
    model_name = 'LQR_recurrent_mdl2.h5'
    ctrlr_name = 'LQR_controller_mdl2_%d.h5'
    hist_name = 'LQR_recurrent_hist2.pkl'
    os.makedirs(results_dir, exist_ok=True)

    Q = np.eye(2)*1.0
    Q_f = np.eye(2)*1.0
    R = np.eye(1)*0.01
    horizon = 20
    batch_size = 10

    model = NN.LQR_recurrent(2, 1, Q, R, Q_f, results_dir+fwd_model_name,
                             horizon, True)

    x_0 = np.random.rand(100000, 2)*10
    x_f = np.zeros((100000, 2))

    hist = model.train(x_0, x_f,
                       val_split=0.1,
                       nb_epoch=10000,
                       batch_size=10,
                       verbosity=1)

    model.save(results_dir+model_name)
    model.save_controller(results_dir+ctrlr_name)
    #pkl.dump(hist, open(results_dir+hist_name, 'wb'))
