'''
This script tests the learned LQR controller
'''

import numpy as np
import _pickle as pkl
import matplotlib.pyplot as plt
from keras.models import load_model

if __name__=='__main__':
    ctrl_path = '../../../data/LQRSys/results/LQR_controller_mdl2_%d.h5'
    sys_data_path = '../../../data/LQRSys/datasets/LQR_babel_parameters.pkl'

    ctrl = []
    m, k, c, del_t, Q, R = pkl.load(open(sys_data_path, 'rb'))
    n = 20

    for i in range(n):
        ctrl.append(load_model(ctrl_path%i))

    A = np.array([[0.0, 1.0],  [-k/m, -c/m]])
    B = np.array([0.0, 1.0/m])

    A_d = np.eye(2) + del_t*A
    B_d = del_t*B

    x = np.zeros((2, n+1))
    u = np.zeros(n)
    t = np.linspace(0.0, n*del_t, n+1)

    x[:, 0] = np.array([1, 0])

    for i in range(n):
        u[i] = ctrl[i].predict(x[:, i].reshape((1, 2)))
        x[:, i+1] = A_d@x[:, i] + B_d*u[i]

    plt.plot(t, x.T)
    plt.xlabel('time')
    plt.ylabel('state')
    plt.figure()
    plt.plot(t[1:], u)
    plt.xlabel('time')
    plt.ylabel('input')
    plt.show()
