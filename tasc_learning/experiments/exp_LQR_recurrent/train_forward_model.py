'''
This file trains the forward model of the LQR system on motor bable
data
'''

import numpy as np
import os
import tasc_learning.datasets.LQRSys as dset
import tasc_learning.models.lin_forward as NN
import _pickle as pkl

if __name__ == '__main__':
    data = dset.LQRSys('LQR_babel_dataset1.pkl')
    results_dir = '../../../data/LQRSys/results/'
    model_name = 'LQR_forward_mdl1.h5'
    hist_name = 'LQR_forward_hist1.pkl'
    os.makedirs(results_dir, exist_ok=True)

    model = NN.Lin_Forward(2, 1)
    hist = model.train([data.x_t, data.u], data.x_t1,
                       val_split=0.1,
                       epochs=100,
                       batch_size=10,
                       verbosity=1)

    model.save(results_dir+model_name)
    pkl.dump(hist, open(results_dir+hist_name, 'wb'))
