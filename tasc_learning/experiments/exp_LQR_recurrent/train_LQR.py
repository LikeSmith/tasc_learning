'''
This file trains the LQR controller
'''

import numpy as np
import os
import tasc_learning.models.LQR_recurrent as NN
import _pickle as pkl

if __name__ == '__main__':
    results_dir = '../../../data/LQRSys/results/'
    fwd_model_name = 'LQR_forward_mdl1.h5'
    model_params_name = '../datasets/LQR_babel_parameters1.pkl'
    model_name = 'LQR_recurrent_mdl1.h5'
    ctrlr_name = 'LQR_controller_mdl1.h5'
    hist_name = 'LQR_recurrent_hist1.pkl'
    os.makedirs(results_dir, exist_ok=True)

    Q = np.array([[10.0, 0.0], [0.0, 1.0]])*0.1
    Q_f = Q
    R = np.eye(1)*0.1*0.1
    horizon = 20
    batch_size = 10

    model = NN.LQR_recurrent(2, 1, Q, R, Q_f, results_dir+fwd_model_name,
                             horizon)
    #model = NN.LQR_recurrent(2, 1, Q, R, Q_f, results_dir+model_params_name,
    #                         horizon, from_params=True, model_type=1)

    #K_opt = np.array([-9.8737, -5.9552])
    #K_opt = np.array([-0.0012627, -0.05469214])
    #model.set_controller(K_opt)
    #model.save_controller(results_dir+'LQR_controller_init.h5')

    x_0 = np.random.rand(10000, 2)*10
    x_f = np.zeros((10000, 2))
    #x_f = model.get_full_output(10000)

    hist = model.train(x_0, x_f,
                       val_split=0.1,
                       epochs=10000,
                       batch_size=1,
                       verbosity=1)

    model.save(results_dir+model_name)
    model.save_controller(results_dir+ctrlr_name)
    #model.save_forward_model(results_dir+'LQR_forward_true.h5')
    #pkl.dump(hist, open(results_dir+hist_name, 'wb'))'''
