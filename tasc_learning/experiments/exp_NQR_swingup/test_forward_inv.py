'''
script to test the forward inverted pendulum model
'''

import numpy as np
import tasc_learning.datasets.pendSys as dset
import tasc_learning.models.nonlin_forward as NN
import tasc_learning.datasets.generate.pend_dataGen as dGen
import matplotlib.pyplot as plt

if __name__=='__main__':
    data = dset.PendSys(setname='invpend_dataset2_data.pkl',
                        parname='invpend_dataset2_param.pkl')
    mdl_path = '../../../data/pendSys/results/invpend_forward_mdl2.h5'

    n_hidden_layers = 10

    x_0 = np.array([0.1, 0.0])
    u = np.array([0.0])
    t_f = 10
    n = 101

    sim = dGen.Pendulum_DataGen(data.m, data.l, data.g, data.c, inv=True)
    mdl = NN.Nonlin_Forward(2, 1, n_hidden_layers, from_file=True, file_path=mdl_path)

    x = np.zeros((n, 2))
    t = np.linspace(0.0, t_f, n)

    x[0, :] = x_0

    for i in range(1, n):
        x[i, :] = mdl.eval(x[i-1, :], u)

    plt.plot(t, x)
    plt.xlabel('time (s)')
    plt.ylabel('learned model state')
    plt.figure()
    sim.plot_res(n, t_f, x_0, u)
