'''
Tests learned NQR model for inverted pendulum
'''

import numpy as np
import tasc_learning.models.mlp as NN
import tasc_learning.models.nonlin_forward as fwd_NN
import tasc_learning.datasets.pendSys as dset
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from scipy import integrate
import _pickle as pkl

def f(t, x, params):
    m, l, c, g, Q, R, u_max, ctrlr = params
    theta = x[0]
    omega = x[1]
    u = u_max*np.tanh(ctrlr.eval(x[0:2])/u_max)
    alpha = u/(m*l**2) + g/l*np.sin(theta) - c/(m*l**2)*omega
    L = theta**2*Q[0, 0] + omega**2*Q[1, 1] + 2*theta*omega*Q[0, 1] + u**2*R[0, 0]
    return np.array([omega, alpha, L])

if __name__ == '__main__':
    data = dset.PendSys(setname='invpend_dataset2_data.pkl',
                        parname='invpend_dataset2_param.pkl')
    results_dir = '../../../data/pendSys/results/'
    model_params_name = '../datasets/invpend_dataset2_param.pkl'
    ctrlr_name = 'invpend_controller_mdl2.h5'
    fwd_mdl_name = 'invpend_forward_mdl2.h5'

    t_f = 10.0
    del_t = 0.1
    x0 = np.array([3.0, 0.0, 0.0])
    Q = np.array([[10.0, 0.0], [0.0, 1.0]])
    R = np.eye(1)*0.1
    u_max = 9.0

    m, l, c, g = pkl.load(open(results_dir+model_params_name, 'rb'))

    ctrlr = NN.MLP(2, 1, 1, [10], ['tanh'], from_file=True,
                   file_path=results_dir+ctrlr_name)
    fwd_mdl = fwd_NN.Nonlin_Forward(2, 1, 10, from_file=True,
                                    file_path=results_dir+fwd_mdl_name)

    n = int(np.floor(t_f/del_t)) + 1
    t = np.linspace(0.0, t_f, n)
    u = np.zeros(n)
    x = np.zeros((3, n))
    u_sim = np.zeros(n)
    x_sim = np.zeros((2, n))
    L_sim = np.zeros(n)
    x[:, 0] = x0
    x_sim[:, 0] = x0[0:2]

    r = integrate.ode(f)
    r.set_integrator('dopri5')
    r.set_initial_value(x0, 0.0)
    r.set_f_params([m, l, c, g, Q, R, u_max, ctrlr])

    for i in range(1, n):
        r.integrate(t[i])
        x[:, i] = r.y
        u[i] = u_max*np.tanh(ctrlr.eval(x[0:2, i-1])/u_max)
        u_sim[i] =  ctrlr.eval(x_sim[:, i-1])
        x_sim[:, i] = data.f_disc([x_sim[:, i-1], u_sim[i]], theano_obj=False)
        u_sim[i] = u_max*np.tanh(u_sim[i]/u_max)

    plt.figure()
    plt.plot(t, x[0:2, :].T)
    plt.xlabel('time (s)')
    plt.ylabel('x')
    plt.savefig(results_dir + 'state_plot.png')
    plt.figure()
    plt.plot(t, x[2, :])
    plt.xlabel('time (s)')
    plt.ylabel('J')
    plt.savefig(results_dir + 'cost_plot.png')
    plt.figure()
    plt.plot(t, u)
    plt.xlabel('time (s)')
    plt.ylabel('u')
    plt.savefig(results_dir + 'input_plog.png')
    plt.figure()
    plt.plot(t, x_sim.T)
    plt.xlabel('time (s)')
    plt.ylabel('x simulated')
    plt.savefig(results_dir + 'state_sim_plot.png')
    plt.figure()
    plt.plot(t, u_sim)
    plt.xlabel('time(s)')
    plt.ylabel('u simulated')
    plt.savefig(results_dir + 'input_sim_plot.png')
    #plt.show()
