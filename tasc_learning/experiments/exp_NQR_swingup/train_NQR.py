'''
Train the recursive nonlinear LQR swingup controller
'''

import numpy as np
import os
import tasc_learning.models.LQR_nonlin as NN
import tasc_learning.datasets.pendSys as dset
import _pickle as pkl
import sys

if __name__=='__main__':
    sys.setrecursionlimit(10000)
    data = dset.PendSys(setname='invpend_dataset2_data.pkl',
                        parname='invpend_dataset2_param.pkl')
    results_dir = '../../../data/pendSys/results/'
    fwd_model_name = 'invpend_forward_mdl2.h5'
    model_params_name = '../datasets/invpend_dataset2_param.pkl'
    model_name = 'invpend_train_mdl2.h5'
    ctrlr_name = 'invpend_controller_mdl2.h5'
    hist_name = 'invpend_recurrent_hist2.pkl'
    os.makedirs(results_dir, exist_ok=True)

    Q = np.array([[10.0, 0.0], [0.0, 1.0]])*0.1
    Q_f = 100*Q
    R = np.eye(1)*0.1*0.1
    horizon = 101
    batch_size = 10
    n_hidden_states_forward = 10
    n_hidden_states_ctrl = (100, 50)

    model = NN.LQR_Nonlin(2, 1, results_dir+fwd_model_name,
                          n_hidden_states_forward, n_hidden_states_ctrl,
                          Q, R, Q_f, horizon, from_dynamics=True,
                          f=data.f_disc)

    x_0_n = np.random.rand(100000, 2)*np.array([6.2832, 0]) - np.array([3.1416, 0])
    #x_0_l = np.random.rand(10000, 2)*np.array([0.2, 8]) - np.array([0.1, 4])
    #x_0 = np.concatenate([x_0_n, x_0_l])
    x_0 = x_0_n
    x_f = np.zeros((100000, 2))

    model.train(x_0, x_f, 0.2, 10000, batch_size, 2, 100)

    model.save(results_dir+model_name)
    model.save_controller(results_dir+ctrlr_name)
