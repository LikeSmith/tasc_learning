#!/bin/bash

# set output and error output filenames
#SBATCH -o NQR_train_%j.out
#SBATCH -e NQR_train_%j.err

# email me when job starts and stops
#SBATCH --mail-type=ALL
#SBATCH --mail-user=crandallk@gwu.edu

# set up which queue should be joined
#SBATCH -N 1
#SBATCH -p gpu

# set the correct directory
#SBATCH -D /home/crandallk/TASC_Learning/tasc_learning/experiments/exp_NQR_swingup

# name job
#SBATCH -J NQR_train

# set time limit
#SBATCH -t 3-00:00:00

# load necesary modules
module load anaconda

# setup environmental variables
export PYTHONPATH=/home/crandallk/TASC_Learning:$PYTHONPATH

# run task
THEANO_FLAGS=optimizer=fast_compile,optimizer_including=fusion python3 train_NQR.py
python3 test_NQR.py
