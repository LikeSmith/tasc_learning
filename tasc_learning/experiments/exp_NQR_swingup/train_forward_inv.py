'''
experiment to train inverted pendulum forward model
'''

import numpy as np
import _pickle as pkl
import os
import tasc_learning.datasets.pendSys as dset
import tasc_learning.models.nonlin_forward as NN

if __name__=='__main__':
    data = dset.PendSys(setname='invpend_dataset2_data.pkl',
                        parname='invpend_dataset2_param.pkl')
    results_dir = '../../../data/pendSys/results/'
    model_name = 'invpend_forward_mdl2.h5'
    hist_name = 'invpend_forward_hist2.pkl'
    os.makedirs(results_dir, exist_ok=True)

    n_hidden_layers = 10

    model = NN.Nonlin_Forward(2, 1, n_hidden_layers)
    hist = model.train([data.x_t, data.u], data.x_t1,
                       val_split=0.1,
                       epochs=100,
                       batch_size=10,
                       verbosity=1)

    model.save(results_dir+model_name)
    pkl.dump(hist, open(results_dir+hist_name, 'wb'))
