import numpy as np
import os
import tasc_learning.datasets.LQRSys as ls
import tasc_learning.models.LQR_basic as NN
import _pickle as p

if __name__ == '__main__':
    data = ls.LQRSys(1)
    results_dir = "../../../data/LQRSys/results/"

    os.makedirs(results_dir, exist_ok=True)

    model = NN.LQR_basic()
    hist = model.train(data.x_t, data.u, 100, 0.2)

    model.get_model().save('%smodel.h5'%results_dir)
    p.dump(hist, open('%shist.pkl'%results_dir, 'wb'))
