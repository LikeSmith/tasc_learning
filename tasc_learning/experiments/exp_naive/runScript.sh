#!/bin/bash

# set output and error output filenames
#SBATCH -o hp_explore_%j.out
#SBATCH -e hp_explore_%j.err

# email me when job starts and stops
#SBATCH --mail-type=ALL
#SBATCH --mail-user=crandallk@gwu.edu

# set up which queue should be joined
#SBATCH -N 1
#SBATCH -p defq

# set the correct directory
#SBATCH -D /groups/sslgrp/Kyle/TASC_Learning/tasc_learning/experiments/exp_naive

# name job
#SBATCH -J hp_explore

# set time limit
#SBATCH -t 3-00:00:00

# load necesary modules
module load openmpi/current
module load anaconda/4.2.0

# run task
mpirun bash setup_par.bash
