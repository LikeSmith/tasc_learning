'''
this script is to test the results from the test where colonial one
failed to save the loss data for the samples.
'''

import numpy as np
from keras.models import Model
from keras.models import load_model
import tasc_learning.datasets.LinSys as ls

if __name__ == '__main__':
    data = ls.LinSys(3)
    n_sets = 1
    n_models = 1

    train_in = [data.xs_t, data.xp_t, data.u_t]
    train_out = [data.xp_t1, data.xs_t1, data.u_t]

    val_loss = np.array((n_sets, n_models))

    for i in range(n_sets):
        for j in range(n_models):
            model_path = '../../../data/results/modelset_1/models_%d/naive_full_netwk_%d.h5'%(i, j)
            print('loading model: %s'%model_path)
            
            mdl = load_model(model_path)

            loss = mdl.evaluate(train_in, train_out, 100)

            print(loss)
            print(mdl.metrics_names)
