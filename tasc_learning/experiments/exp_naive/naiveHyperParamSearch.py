'''
This experiment explores the hyperparameter space
'''

import numpy as np
import _pickle
import os

from mpi4py import MPI
import tasc_learning.datasets.LinSys as ls
import tasc_learning.models.naiveNN_disc as NN

if __name__ == '__main__':
    data = ls.LinSys(2)

    n_abs_states_range = [2, 200]
    n_xs_hidden_range = [10, 1000]
    n_parent_hidden_range = [10, 1000]
    n_swarm_hidden_range = [10, 1000]
    n_u_hidden_range = [10, 1000]
    dropout_rate_range = [0.0, 0.5]
    results_dir = '../../../data/results/exp_naive/'
    n_tests = 2
    batch_size = 100
    nb_epoch = 100

    os.makedirs(results_dir, exist_ok=True)
    p = MPI.COMM_WORLD.Get_rank()

    train_in = [data.xs_t, data.xp_t, data.u_t]
    train_out = [data.xp_t1, data.xs_t1, data.u_t]

    sys_params = [data.n_swarm_states, data.n_swarm_inputs, data.n_parent_states]

    for i in range(n_tests):
        n_abs_states = np.random.random_integers(n_abs_states_range[0], n_abs_states_range[1])
        n_xs_hidden = np.random.random_integers(n_xs_hidden_range[0], n_xs_hidden_range[1])
        n_parent_hidden = np.random.random_integers(n_parent_hidden_range[0], n_parent_hidden_range[1])
        n_swarm_hidden = np.random.random_integers(n_swarm_hidden_range[0], n_swarm_hidden_range[1])
        n_u_hidden = np.random.random_integers(n_u_hidden_range[0], n_u_hidden_range[1])
        dropout_rate = np.random.rand()*(dropout_rate_range[1] - dropout_rate_range[0]) + dropout_rate_range[0]

        hyper_params = [n_abs_states, n_xs_hidden, n_parent_hidden, n_swarm_hidden, n_u_hidden, dropout_rate]

        netwk = NN.naiveNN(sys_params, hyper_params, batch_size)
        hist = netwk.train(train_in, train_out, nb_epoch, 0.2, 1)

        full_model = netwk.get_full_model()
        abstractor = netwk.get_abstractor()
        controller = netwk.get_controller()

        full_model.save('%sfull_model_network_%d_%d.h5'%(results_dir, p, i))
        abstractor.save('%sabstractor_network_%d_%d.h5'%(results_dir, p, i))
        controller.save('%scontroller_network_%d_%d.h5'%(results_dir, p, i))

        _pickle.dump([n_abs_states, n_xs_hidden, n_parent_hidden, n_swarm_hidden, dropout_rate, hist.history],
                     open('%sresults_%d_%d.pkl'%(results_dir, p, i), 'wb'))
