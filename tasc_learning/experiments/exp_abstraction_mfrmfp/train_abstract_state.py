'''
This script will train an abstract state for the MFRMFP system
'''

import numpy as np
import os
import tasc_learning.models.abstractionEncoder as NN
import tasc_learning.datasets.MFRMFPSys as data
import _pickle as pkl

if __name__=='__main__':
    results_dir = '../../../data/MFRMFPSys/results/'
    abs_enc_file = 'MFRMFP_abs_enc_mdl1.h5'
    abs_dyn_file = 'MFRMFP_abs_dyn_mdl1.h5'
    abs_enc_params = 'MFRMFP_abs_enc_params1.pkl'
    abs_dyn_params = 'MFRMFP_abs_dyn_params1.pkl'
    hist_file = 'MFRMFP_abs_hist1.pkl'
    training_dataset = 'dataset2_4Rob_noctrl_data.pkl'
    training_params = 'dataset2_4Rob_noctrl_params.pkl'
    training_limits = 'dataset2_4Rob_noctrl_limits.pkl'

    abs_enc_layers = [10]
    abs_enc_activ = ['tanh']
    abs_enc_activ_end = 'linear'

    abs_dyn_layers = [10]
    abs_dyn_activ = ['tanh']
    abs_dyn_activ_end = 'linear'

    n_abs_states = 2

    os.makedirs(results_dir, exist_ok=True)

    train_dat = data.MFRMFPSys(training_params, training_limits)
    train_dat.load_data(training_dataset)

    abs_enc_trainer = NN.AbstractionEncoder(2, train_dat.N*2, n_abs_states, abs_enc_layers, abs_enc_activ, abs_enc_activ_end, abs_dyn_layers, abs_dyn_activ, abs_dyn_activ_end)
    abs_enc_trainer.set_home(results_dir)

    hist = abs_enc_trainer.train(train_dat.xp, train_dat.xs, train_dat.xp_new, batch_size=1000, patience=1, verbosity=1)

    abs_enc_trainer.save(abs_enc_file, abs_enc_params, abs_dyn_file, abs_dyn_params)

    pkl.dump(hist, open(results_dir+hist_file, 'wb'))

    print('Done!')
